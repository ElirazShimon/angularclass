import { Component, OnInit } from '@angular/core';
import {InvoicesService} from './invoices.service';
@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.component.html',styles: [`
    .invoices li { cursor: pointer; }
    .invoices li:hover { background: #ecf0f1; }
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }     
  `]
  //styleUrls: ['./invoices.component.css']
})
export class InvoicesComponent implements OnInit {

  isLoading:Boolean = true;
  invoices; 
  currentInvoice;

select(invoice){
    this.currentInvoice = invoice;
  }



  constructor(private _invoicesService:InvoicesService) { }
   addUser(invoice){
     
      this._invoicesService.addInvoice(invoice);
   }
   
   updateUser(invoice){
     this._invoicesService.updateInvoice(invoice);
   }

   deleteUser(invoice){
     this._invoicesService.deleteInvoice(invoice);
  }
  ngOnInit() {
    this._invoicesService.getInvoices().subscribe(invoicesData => 
    {
      this.invoices = invoicesData;
      this.isLoading = false});
  }
    }


