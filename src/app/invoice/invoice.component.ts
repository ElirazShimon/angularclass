import { Component, OnInit,  Output ,EventEmitter} from '@angular/core';
import { Invoice } from './invoice';
@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.css'],
  inputs:['invoice']
})
export class InvoiceComponent implements OnInit {

invoice:Invoice;
@Output() deleteEvent = new EventEmitter<Invoice>();
@Output() editEvent = new EventEmitter<Invoice>();

tempInvoice:Invoice = {amount:null,name:null};
isEdit:Boolean = false;
  editButtonText = 'Edit';
  constructor() { }

  sendDelete(){
   this.deleteEvent.emit(this.invoice);
  }

  cancelEdit(){
    this.isEdit = false;
    this.invoice.amount = this.tempInvoice.amount;
    this.invoice.name = this.tempInvoice.name;
    this.editButtonText = 'Edit'; 
  }
  
  toggleEdit(){
    this.isEdit = !this.isEdit;
    this.isEdit? this.editButtonText = 'Save' : this.editButtonText = 'Edit';
    if(!this.isEdit){
       this.tempInvoice.amount = this.invoice.amount;
       this.tempInvoice.name = this.invoice.name;
     } else {     
       this.editEvent.emit(this.invoice);
     }
  }


  ngOnInit() {
  }

}


