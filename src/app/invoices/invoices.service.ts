import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import {AngularFire} from 'angularfire2';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';

@Injectable()
export class InvoicesService {








  //private _url = 'https://elirazproject.firebaseio.com/';
  //private _url = 'http://elirazit.myweb.jce.ac.il/api/users';


  invoicesObservable;

  getInvoices(){
    //return this._http.get(this._url).map(res =>res.json()).delay(2000)
    this.invoicesObservable =  this.af.database.list('/invoices');
     return this.invoicesObservable;
  }

  


  addInvoice(invoice){
    this.invoicesObservable.push(invoice);
  }


  updateInvoice(invoice){
   let invoiceKey = invoice.$key;
   let invoiceData = {name:invoice.name,amount:invoice.amount};
   this.af.database.object('/invoices/' + invoiceKey).update(invoiceData);
  }

  deleteInvoice(invoice){
    let invoiceKey = invoice.$key;
    this.af.database.object('/invoices/' + invoiceKey).remove();
  }
  constructor(private af:AngularFire) { }

}
 